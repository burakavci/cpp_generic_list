#pragma once
#include <cstdlib>

template <typename T>
class List
{
private:
    T *arrayPointer;
    int arraySize;

public:
    void Add(T item);
    bool Remove(T item);
    void RemoveAt(int index);
    int Count();
    void Clear();
    bool Contains(T item);
    int IndexOf(T item);
    int LastIndexOf(T item);
    void Reverse();
    void Insert(int index, T item);
    T &operator[](int i);
    List();
};