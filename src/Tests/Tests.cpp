#include <iostream>
#include "../List/List.cpp"

using namespace std;

bool AddTest1();
bool AddTest2();
bool AddTest3_MallocConstruct();
bool RemoveAtTest1();
bool RemoveAtTest2();
bool OperatorSquareBracketsTest1();
bool OperatorSquareBracketsTest2();
bool RemoveTest1();
bool RemoveTest2();
bool RemoveTest3();
bool CountTest1();
bool CountTest2();
bool InsertTest1();
bool InsertTest2();

int main()
{
    // cout << "test" << flush;
    // cout.flush();
    cout << "AddTest1()" << endl;
    cout << AddTest1() << endl;
    cout << "AddTest2()" << endl;
    cout << AddTest2() << endl;
    cout << "AddTest3_MallocConstruct()" << endl;
    cout << AddTest3_MallocConstruct() << endl;
    cout << "RemoveAtTest1()" << endl;
    cout << RemoveAtTest1() << endl;
    cout << "RemoveAtTest2()" << endl;
    cout << RemoveAtTest2() << endl;
    cout << "OperatorSquareBracketsTest1()" << endl;
    cout << OperatorSquareBracketsTest1() << endl;
    cout << "OperatorSquareBracketsTest2()" << endl;
    cout << OperatorSquareBracketsTest2() << endl;
    cout << "RemoveTest1()" << endl;
    cout << RemoveTest1() << endl;
    cout << "RemoveTest2()" << endl;
    cout << RemoveTest2() << endl;
    cout << "RemoveTest3()" << endl;
    cout << RemoveTest3() << endl;
    cout << "CountTest1()" << endl;
    cout << CountTest1() << endl;
    cout << "CountTest2()" << endl;
    cout << CountTest2() << endl;
    cout << "InsertTest1()" << endl;
    cout << InsertTest1() << endl;
    cout << "InsertTest2()" << endl;
    cout << InsertTest2() << endl;

    return 0;
}

bool AddTest1()
{
    List<int> *list = new List<int>();
    list->Add(3);
    return (*list)[0] == 3 && (*list).Count() == 1;
}

bool AddTest2()
{
    List<int> *list = new List<int>();
    list->Add(3);
    list->Add(4);
    return (*list)[0] == 3 && (*list)[1] == 4 && (*list).Count() == 2;
}

bool AddTest3_MallocConstruct()
{
    List<int> *list = (List<int> *)malloc(sizeof(List<int>));
    List<int> listInStack;
    *list = listInStack;
    list->Add(3);
    list->Add(4);
    return (*list)[0] == 3 && (*list)[1] == 4 && (*list).Count() == 2;
}

bool RemoveAtTest1()
{
    List<int> *list = new List<int>();
    for (int i = 0; i < 15; i++)
    {
        list->Add(i * 2);
    }
    list->RemoveAt(14);
    bool expected = true;
    for (int i = 0; i < 14; i++)
    {
        int value = list->operator[](i);
        if (value != i * 2)
        {
            expected = false;
            break;
        }
    }
    int count = list->Count();
    return expected && count == 14;
}

bool RemoveAtTest2()
{
    List<int> *list = new List<int>();
    for (int i = 0; i < 15; i++)
    {
        list->Add(i * 2);
    }
    list->RemoveAt(13);
    bool expected = true;
    for (int i = 0; i < 14; i++)
    {
        if (i != 13)
        {
            int value = list->operator[](i);
            if (value != i * 2)
            {
                expected = false;
                break;
            }
        }
        else
        {
            int value = list->operator[](i);
            if (value != (i + 1) * 2)
            {
                expected = false;
                break;
            }
        }
    }
    int count = list->Count();
    return expected && count == 14;
}

bool OperatorSquareBracketsTest1()
{
    List<int> *list = new List<int>();
    list->Add(3);
    (*list)[0] = 4;
    return (*list)[0] == 4 && (*list).Count() == 1;
}

bool OperatorSquareBracketsTest2()
{
    List<int> *list = new List<int>();
    list->Add(3);
    for (int i = 0; i < 15; i++)
    {
        list->Add(i * 2 * 3);
    }

    list->operator[](15) = 4;
    return (*list)[15] == 4 && (*list).Count() == 16;
}

bool RemoveTest1()
{
    List<int> *list = new List<int>();
    for (int i = 0; i < 15; i++)
    {
        list->Add(i * 2);
    }
    bool removed = list->Remove(28);
    bool expected = true;
    for (int i = 0; i < 14; i++)
    {
        int value = list->operator[](i);
        if (value != i * 2)
        {
            expected = false;
            break;
        }
    }
    int count = list->Count();
    return expected && count == 14 && removed;
}

bool RemoveTest2()
{
    List<int> *list = new List<int>();
    for (int i = 0; i < 15; i++)
    {
        list->Add(i * 2);
    }
    bool removed = list->Remove(26);
    bool expected = true;
    for (int i = 0; i < 14; i++)
    {
        if (i != 13)
        {
            int value = list->operator[](i);
            if (value != i * 2)
            {
                expected = false;
                break;
            }
        }
        else
        {
            int value = list->operator[](i);
            if (value != (i + 1) * 2)
            {
                expected = false;
                break;
            }
        }
    }
    int count = list->Count();
    return expected && count == 14 && removed;
}

bool RemoveTest3()
{
    List<int> *list = new List<int>();
    for (int i = 0; i < 15; i++)
    {
        list->Add(i * 2);
    }
    bool removed = list->Remove(27);
    bool expected = true;
    for (int i = 0; i < 14; i++)
    {
        int value = list->operator[](i);
        if (value != i * 2)
        {
            expected = false;
            break;
        }
    }
    int count = list->Count();
    return expected && count == 15 && !removed;
}

bool CountTest1()
{
    List<int> *list = new List<int>();
    for (int i = 0; i < 15; i++)
    {
        list->Add(i * 2);
    }
    int count = list->Count();
    return count == 15;
}

bool CountTest2()
{
    List<int> *list = new List<int>();
    int count = list->Count();
    return count == 0;
}

bool InsertTest1()
{
    List<int> *list = new List<int>();
    for (int i = 0; i < 15; i++)
    {
        list->Add(i * 2);
    }
    int expectedArray[] = {0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 12345, 28};
    list->Insert(list->Count() - 1, 12345);
    bool expected = true;
    for (int i = 0; i < 16; i++)
    {
        int value = list->operator[](i);
        if (value != expectedArray[i])
        {
            expected = false;
            break;
        }
    }
    int count = list->Count();
    return expected && count == 16;
}

bool InsertTest2()
{
    List<int> *list = new List<int>();
    for (int i = 0; i < 15; i++)
    {
        list->Add(i * 2);
    }
    int expectedArray[] = {123456, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28};
    list->Insert(0, 123456);
    bool expected = true;
    for (int i = 0; i < 16; i++)
    {
        int value = list->operator[](i);
        if (value != expectedArray[i])
        {
            expected = false;
            break;
        }
    }
    int count = list->Count();
    return expected && count == 16;
}